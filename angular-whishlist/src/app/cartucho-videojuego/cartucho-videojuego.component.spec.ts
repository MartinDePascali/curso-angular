import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CartuchoVideojuegoComponent } from './cartucho-videojuego.component';

describe('CartuchoVideojuegoComponent', () => {
  let component: CartuchoVideojuegoComponent;
  let fixture: ComponentFixture<CartuchoVideojuegoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CartuchoVideojuegoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CartuchoVideojuegoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
