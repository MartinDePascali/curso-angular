import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCartuchosComponent } from './lista-cartuchos.component';

describe('ListaCartuchosComponent', () => {
  let component: ListaCartuchosComponent;
  let fixture: ComponentFixture<ListaCartuchosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaCartuchosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCartuchosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
