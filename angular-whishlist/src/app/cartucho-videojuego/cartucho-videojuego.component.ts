import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Cartuchos } from '../models/cartuchos.model';

@Component({
  selector: 'app-cartucho-videojuego',
  templateUrl: './cartucho-videojuego.component.html',
  styleUrls: ['./cartucho-videojuego.component.css']
})
export class CartuchoVideojuegoComponent implements OnInit {

  @Input() cartuchoJuego: Cartuchos;
  @Input() contacto: string;
  @HostBinding('attr.class') cssClass = 'col-md-4';

  constructor() { 
  }

  ngOnInit(): void {
  }

}
