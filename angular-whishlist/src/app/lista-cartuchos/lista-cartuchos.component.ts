import { Component, OnInit, Input } from '@angular/core';
import { Cartuchos } from '../models/cartuchos.model';

@Component({
  selector: 'app-lista-cartuchos',
  templateUrl: './lista-cartuchos.component.html',
  styleUrls: ['./lista-cartuchos.component.css']
})
export class ListaCartuchosComponent implements OnInit {

  cartuchos: Cartuchos[];
  titulo = 'Mi whishlist';
  contactos : string[];

  constructor() { 
    this.cartuchos = [];

    this.contactos = ['Twitter', 'Facebook', 'Instagram'];
  }

  ngOnInit(): void {
  }

  guardar(nombre:string, url:string):boolean {
    this.cartuchos.push(new Cartuchos(nombre, url));
    console.log(this.cartuchos);
    return false;
  }

}
