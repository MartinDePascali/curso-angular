import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CartuchoVideojuegoComponent } from './cartucho-videojuego/cartucho-videojuego.component';
import { ListaCartuchosComponent } from './lista-cartuchos/lista-cartuchos.component';

@NgModule({
  declarations: [
    AppComponent,
    CartuchoVideojuegoComponent,
    ListaCartuchosComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
